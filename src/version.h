/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILUR_EP_VERSION_H
#define FILUR_EP_VERSION_H

#define VALUE_TO_STRING(value) #value
#define VER_TO_STRING(version) VALUE_TO_STRING(version)

#ifndef MAJOR_VERSION
  #define MAJOR_VERSION 0
#endif

#ifndef MINOR_VERSION
  #define MINOR_VERSION 0
#endif

#ifndef PATCH_VERSION
  #define PATCH_VERSION 0
#endif

#ifdef BUILD_VERSION
  #define RC_BUILD_NUMBER BUILD_VERSION
#else
  #define RC_BUILD_NUMBER 0
#endif

#define VERSION_STR VER_TO_STRING(MAJOR_VERSION) "." VER_TO_STRING(MINOR_VERSION) "." VER_TO_STRING(PATCH_VERSION)

#if(defined BUILD_VERSION && defined HASH_VERSION)
  #define FULL_VERSION_STR VERSION_STR "-" VER_TO_STRING(BUILD_VERSION) "-" VER_TO_STRING(HASH_VERSION)
#elif(defined BUILD_VERSION)
  #define FULL_VERSION_STR VERSION_STR "-" VER_TO_STRING(BUILD_VERSION)
#elif(defined HASH_VERSION)
  #define FULL_VERSION_STR VERSION_STR "-" VER_TO_STRING(HASH_VERSION)
#else
  #define FULL_VERSION_STR VERSION_STR
#endif

#define stringPluginName        "filurep"
#define stringOriginalFilename  "filurep.vst3"
#if PLATFORM_64
#define stringFileDescription   "filurep (64Bit)"
#else
#define stringFileDescription   "filurep"
#endif
#define stringCompanyName       "Andreas Ersson"
#define stringCompanyWeb        "https://gitlab.com/andreasersson/filurep-vst"
#define stringCompanyEmail      ""

#define stringLegalCopyright    "© 2018-2021 Andreas Ersson"
#define stringLegalTrademarks   "VST is a trademark of Steinberg Media Technologies GmbH"

#endif // FILUR_EP_VERSION_H
