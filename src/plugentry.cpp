/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "controller.h"
#include "processor.h"
#include "version.h"

#include <public.sdk/source/main/pluginfactory.h>

static Steinberg::FUnknown* createProcessorInstance(void*) {
  return static_cast<Steinberg::Vst::IAudioProcessor*>(new ep::Processor(ep::Controller::cid));
}

static Steinberg::FUnknown* createControllerInstance(void*) {
  return static_cast<Steinberg::Vst::IEditController*>(new ep::Controller());
}

BEGIN_FACTORY_DEF(stringCompanyName, stringCompanyWeb,  stringCompanyEmail)

    DEF_CLASS2(INLINE_UID_FROM_FUID(ep::Processor::cid),
               PClassInfo::kManyInstances,
               kVstAudioEffectClass,
               stringPluginName,
               Vst::kDistributable,
               Vst::PlugType::kInstrumentSynth,
               FULL_VERSION_STR,
               kVstVersionString,
               createProcessorInstance)

    DEF_CLASS2(INLINE_UID_FROM_FUID(ep::Controller::cid),
               PClassInfo::kManyInstances,
               kVstComponentControllerClass,
               stringPluginName,
               0,
               "",
               FULL_VERSION_STR, kVstVersionString,
               createControllerInstance)END_FACTORY

bool InitModule() {
  return true;
}
bool DeinitModule() {
  return true;
}
