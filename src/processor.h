/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILUR_EP_PROCESSOR_H
#define FILUR_EP_PROCESSOR_H

#include "parameter_state.h"
#include <filur/ep/ep_parameters.h>
#include <filur/ep/ep.h>
#include <filur/ep/eq.h>
#include <filur/ep/wah.h>
#include <filur/ep/tremolo.h>

#include "public.sdk/source/vst/vstaudioeffect.h"

#include <vector>

namespace ep {

class Processor : public Steinberg::Vst::AudioEffect {
 public:
  explicit Processor(const Steinberg::FUID& controller_id);

  Steinberg::tresult PLUGIN_API initialize(Steinberg::FUnknown* context) SMTG_OVERRIDE;
  Steinberg::tresult PLUGIN_API setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                                   Steinberg::int32 numIns,
                                                   Steinberg::Vst::SpeakerArrangement* outputs,
                                                   Steinberg::int32 numOuts) SMTG_OVERRIDE;

  Steinberg::tresult PLUGIN_API setState(Steinberg::IBStream* state_stream) SMTG_OVERRIDE;
  Steinberg::tresult PLUGIN_API getState(Steinberg::IBStream* state_stream) SMTG_OVERRIDE;

  Steinberg::tresult PLUGIN_API canProcessSampleSize(Steinberg::int32 symbolic_sample_size) SMTG_OVERRIDE;
  Steinberg::tresult PLUGIN_API setActive(Steinberg::TBool state) SMTG_OVERRIDE;
  Steinberg::tresult PLUGIN_API process(Steinberg::Vst::ProcessData& data) SMTG_OVERRIDE;

  static Steinberg::FUID cid;

 private:
  ParameterState m_state;
  ep_parameters_t m_parameters = {};
  eq_parameters_t m_eq_parameters = {};
  wah_parameters_t m_wah_parameters = {};
  tremolo_parameters_t m_tremolo_parameters = {};

  ep_t m_ep = {};
  eq_t m_eq = {};
  wah_t m_wah = {};
  tremolo_t m_tremolo = {};
  std::vector<double> m_buffer;

  Steinberg::TBool m_is_active = {0};
};

}  // namespace ep

#endif // FILUR_EP_PROCESSOR_H
