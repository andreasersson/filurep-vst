/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "processor.h"

#include "parameter_ids.h"

#include <filur/tempo_sync.h>

#include <pluginterfaces/vst/ivstevents.h>
#include <pluginterfaces/vst/ivstparameterchanges.h>

#include <algorithm>
#include <cassert>

namespace ep {

Steinberg::FUID Processor::cid(0x4BF47DDF, 0x8CD74C65, 0x9BF75AC4, 0xB145FCD0);

static constexpr double eq_default_resonance = 0.707;

static wah_enable_t value_to_wah_enable(Steinberg::Vst::ParamValue value) {
  wah_enable_t enable = WAH_OFF;
  if (value >= 0.5) {
    enable = WAH_ON;
  }

  return enable;
}

static wah_lfo_enable_t value_to_wah_lfo_enable(Steinberg::Vst::ParamValue value) {
  wah_lfo_enable_t enable = WAH_LFO_OFF;
  if (value >= 0.5) {
    enable = WAH_LFO_ON;
  }

  return enable;
}

static tremolo_enable_t value_to_tremolo_enable(Steinberg::Vst::ParamValue value) {
  tremolo_enable_t enable = TREMOLO_OFF;
  if (value >= 0.5) {
    enable = TREMOLO_ON;
  }

  return enable;
}

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                                     ParameterState& state,
                                     ep_parameters_t& parameters,
                                     eq_parameters_t& eq_parameters,
                                     wah_parameters_t& wah_parameters,
                                     tremolo_parameters_t& tremolo_parameters);
static void set_parameter(Steinberg::Vst::ParamID parameter_id,
                          double value,
                          ep_parameters_t& parameters,
                          eq_parameters_t& eq_parameters,
                          wah_parameters_t& wah_parameters,
                          tremolo_parameters_t& tremolo_parameters);
static void set_state_parameters(const ParameterState& state,
                                 ep_parameters_t& parameters,
                                 eq_parameters_t& eq_parameters,
                                 wah_parameters_t& wah_parameters,
                                 tremolo_parameters_t& tremolo_parameters);
static void update_tempo(Steinberg::Vst::ProcessContext* process_context,
                         wah_parameters_t& wah_parameters,
                         tremolo_parameters_t& tremolo_parameters);
static void to_sample32(const double* buffer, const Steinberg::Vst::ProcessData& data);
static void to_sample64(const double* buffer, const Steinberg::Vst::ProcessData& data);

Processor::Processor(const Steinberg::FUID& controller_id) {
  setControllerClass(controller_id);
  processContextRequirements.needTempo();
}

Steinberg::tresult Processor::initialize(Steinberg::FUnknown* context) {
  Steinberg::tresult ret_val = AudioEffect::initialize(context);
  if (Steinberg::kResultTrue == ret_val) {
    addAudioOutput(STR16("output"), Steinberg::Vst::SpeakerArr::kStereo);
    addEventInput(STR16("event Input"), 1);
  }

  return ret_val;
}

Steinberg::tresult Processor::setState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = m_state.setState(state);

  if (m_is_active) {
    set_state_parameters(m_state, m_parameters, m_eq_parameters, m_wah_parameters, m_tremolo_parameters);
  }

  return ret_val;
}

Steinberg::tresult Processor::getState(Steinberg::IBStream* state) {
  Steinberg::tresult ret_val = m_state.getState(state);

  return ret_val;
}

Steinberg::tresult Processor::setBusArrangements(Steinberg::Vst::SpeakerArrangement* inputs,
                                                 Steinberg::int32 num_inputs,
                                                 Steinberg::Vst::SpeakerArrangement* outputs,
                                                 Steinberg::int32 num_outputs) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((0 == num_inputs) && (1 == num_outputs)
      && (Steinberg::Vst::SpeakerArr::kStereo == outputs[0])) {
    ret_val = AudioEffect::setBusArrangements(inputs, num_inputs, outputs, num_outputs);
  }

  return ret_val;
}

Steinberg::tresult Processor::canProcessSampleSize(Steinberg::int32 symbolic_sample_size) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;

  if ((Steinberg::Vst::kSample32 == symbolic_sample_size)
      || (Steinberg::Vst::kSample64 == symbolic_sample_size)) {
    ret_val = Steinberg::kResultTrue;
  }

  return ret_val;
}

Steinberg::tresult Processor::setActive(Steinberg::TBool state) {
  m_is_active = state;
  if (state) {
    ep_parameters_init(&m_parameters);
    ep_init(processSetup.sampleRate, &m_ep);
    eq_init(processSetup.sampleRate, &m_eq);
    wah_init(processSetup.sampleRate, &m_wah);
    tremolo_init(&m_tremolo);
    eq_parameters_init(&m_eq_parameters);
    eq_low_shelf_set_resonance(eq_default_resonance, &m_eq_parameters);
    eq_high_shelf_set_resonance(eq_default_resonance, &m_eq_parameters);

    wah_parameters_init(processSetup.sampleRate, &m_wah_parameters);
    tremolo_parameters_init(processSetup.sampleRate, &m_tremolo_parameters);

    m_buffer.resize(processSetup.maxSamplesPerBlock);

    set_state_parameters(m_state, m_parameters, m_eq_parameters, m_wah_parameters, m_tremolo_parameters);
  }

  return AudioEffect::setActive(state);
}

Steinberg::tresult Processor::process(Steinberg::Vst::ProcessData& data) {
  Steinberg::tresult result = Steinberg::kResultTrue;

  handle_parameter_changes(data.inputParameterChanges,
                           m_state,
                           m_parameters,
                           m_eq_parameters,
                           m_wah_parameters,
                           m_tremolo_parameters);

  if ((data.numOutputs < 1) || (data.outputs[0].numChannels < 2)) {
    return Steinberg::kResultTrue;
  }

  Steinberg::int32 number_of_events = 0;
  Steinberg::Vst::IEventList* input_events = data.inputEvents;
  if (nullptr != input_events) {
    number_of_events = input_events->getEventCount();
  }

  update_tempo(data.processContext, m_wah_parameters, m_tremolo_parameters);

  assert(static_cast<size_t>(data.numSamples) <= m_buffer.size());
  double* out_buffer = m_buffer.data();
  memset(out_buffer, 0, m_buffer.size() * sizeof(double));

  Steinberg::int32 number_of_frames = data.numSamples;
  Steinberg::int32 frame_offset = 0;
  Steinberg::Vst::Event event;
  size_t active = ep_num_active_voices(&m_ep);

  for (Steinberg::int32 i = 0; i < number_of_events; i++) {
    Steinberg::tresult handle_event = input_events->getEvent(i, event);

    if (Steinberg::kResultTrue == handle_event) {
      switch (event.type) {
        // supported event types
        case Steinberg::Vst::Event::kNoteOnEvent:
        case Steinberg::Vst::Event::kNoteOffEvent:
          break;

        // ignore all other event types
        default:
          handle_event = Steinberg::kResultFalse;
          break;
      }
    }

    if (Steinberg::kResultTrue == handle_event) {
      Steinberg::int32 number_of_sub_frames = event.sampleOffset - frame_offset;
      frame_offset = event.sampleOffset;
      number_of_sub_frames = std::min(number_of_sub_frames, number_of_frames);

      if (number_of_sub_frames > 0) {
        ep_process(out_buffer, number_of_sub_frames, &m_parameters, &m_ep);
        eq_process(out_buffer, out_buffer, number_of_sub_frames, &m_eq_parameters, &m_eq);
        wah_process(out_buffer, out_buffer, number_of_sub_frames, &m_wah_parameters, &m_wah);
        tremolo_process(out_buffer, out_buffer, number_of_sub_frames, &m_tremolo_parameters, &m_tremolo);

        out_buffer += number_of_sub_frames;
        number_of_frames -= number_of_sub_frames;
        active += ep_num_active_voices(&m_ep);
      }

      switch (event.type) {
        case Steinberg::Vst::Event::kNoteOnEvent:
          // restart wah and tremolo lfo:s if no active voices
          if (0 == ep_num_active_voices(&m_ep)) {
            wah_restart_lfo(&m_wah_parameters, &m_wah);
            tremolo_restart_lfo(&m_tremolo);
          }
          ep_note_on(event.noteOn.pitch, event.noteOn.velocity, &m_parameters, &m_ep);
          break;

        case Steinberg::Vst::Event::kNoteOffEvent:
          ep_note_off(event.noteOff.pitch, &m_parameters, &m_ep);
          break;

        default:
          break;
      }
    }
  }

  if (number_of_frames > 0) {
    ep_process(out_buffer, number_of_frames, &m_parameters, &m_ep);
    eq_process(out_buffer, out_buffer, number_of_frames, &m_eq_parameters, &m_eq);
    wah_process(out_buffer, out_buffer, number_of_frames, &m_wah_parameters, &m_wah);
    tremolo_process(out_buffer, out_buffer, number_of_frames, &m_tremolo_parameters, &m_tremolo);
    active += ep_num_active_voices(&m_ep);
  }

  if (Steinberg::Vst::kSample64 == data.symbolicSampleSize) {
    to_sample64(m_buffer.data(), data);
  } else {
    to_sample32(m_buffer.data(), data);
  }

  if (0 == active) {
    data.outputs[0].silenceFlags = 0x03;
  }

  return result;
}

static void handle_parameter_changes(Steinberg::Vst::IParameterChanges* parameter_changes,
                              ParameterState& state,
                              ep_parameters_t& parameters,
                              eq_parameters_t& eq_parameters,
                              wah_parameters_t& wah_parameters,
                              tremolo_parameters_t& tremolo_parameters) {
  Steinberg::int32 parameter_count = 0;
  if (nullptr != parameter_changes) {
    parameter_count = parameter_changes->getParameterCount();
  }

  for (Steinberg::int32 i = 0; i < parameter_count; i++) {
    Steinberg::tresult result = Steinberg::kResultTrue;
    Steinberg::Vst::IParamValueQueue* queue = parameter_changes->getParameterData(i);
    if (nullptr == queue) {
      result = Steinberg::kResultFalse;
    }

    if ((Steinberg::kResultTrue == result) && (0 == queue->getPointCount())) {
      result = Steinberg::kResultFalse;
    }

    Steinberg::Vst::ParamValue value = 0;
    Steinberg::int32 sample_offset = 0;
    if (Steinberg::kResultTrue == result) {
      result = queue->getPoint(queue->getPointCount() - 1, sample_offset, value);
    }

    if (Steinberg::kResultTrue == result) {
      state.setNormalized(queue->getParameterId(), value);
      set_parameter(queue->getParameterId(),
                    state.get(queue->getParameterId()),
                    parameters,
                    eq_parameters,
                    wah_parameters,
                    tremolo_parameters);
    }
  }
}

static void set_parameter(Steinberg::Vst::ParamID parameter_id,
                          double value,
                          ep_parameters_t& parameters,
                          eq_parameters_t& eq_parameters,
                          wah_parameters_t& wah_parameters,
                          tremolo_parameters_t& tremolo_parameters) {
  switch (parameter_id) {
    case kVolume:
      ep_set_volume(value, &parameters);
      break;

    case kDeprecatedOverdrive:
      break;

    case kEqLowShelfCutoff:
      eq_low_shelf_set_cutoff(value, &eq_parameters);
      break;

    case kEqLowShelfResonance:
      eq_low_shelf_set_resonance(value, &eq_parameters);
      break;

    case kEqLowShelfGain:
      eq_low_shelf_set_gain(value, &eq_parameters);
      break;

    case kEqHighShelfCutoff:
      eq_high_shelf_set_cutoff(value, &eq_parameters);
      break;

    case kEqHighShelfResonance:
      eq_high_shelf_set_resonance(value, &eq_parameters);
      break;

    case kEqHighShelfGain:
      eq_high_shelf_set_gain(value, &eq_parameters);
      break;

    case kPickupTimbre:
      ep_set_pickup_timbre(value, &parameters);
      break;

    case kPickupVolume:
      ep_set_pickup_volume(value, &parameters);
      break;

    case kClangVolume:
      ep_set_clang_volume(value, &parameters);
      break;

    case kDecay:
      ep_set_decay(value, &parameters);
      break;

    case kClangDecay:
      ep_set_clang_decay(value, &parameters);
      break;

    case kRelease:
      ep_set_release(value, &parameters);
      break;

    case kWahEnabled:
      wah_set_enabled(value_to_wah_enable(value), &wah_parameters);
      break;

    case kWahCutoff:
      wah_set_cutoff(value, &wah_parameters);
      break;

    case kWahResonance:
      wah_set_resonance(value, &wah_parameters);
      break;

    case kWahLfoEnabled:
      wah_set_lfo_enabled(value_to_wah_lfo_enable(value), &wah_parameters);
      break;

    case kWahLfoSpeed:
      wah_set_speed(value, &wah_parameters);
      break;

    case kWahLfoPhase:
      wah_set_phase(value, &wah_parameters);
      break;

    case kWahTempoSync:
      wah_set_tempo_sync(value_to_tempo_sync(value), &wah_parameters);
      break;

    case kWahLowCutoff:
      wah_set_low_cutoff(value, &wah_parameters);
      break;

    case kWahHighCutoff:
      wah_set_high_cutoff(value, &wah_parameters);
      break;

    case kTremoloEnabled:
      tremolo_set_enabled(value_to_tremolo_enable(value), &tremolo_parameters);
      break;

    case kTremoloLfoSpeed:
      tremolo_set_speed(value, &tremolo_parameters);
      break;

    case kTremoloTempoSync:
      tremolo_set_tempo_sync(value_to_tempo_sync(value), &tremolo_parameters);
      break;

    case kTremoloDepth:
      tremolo_set_depth(value, &tremolo_parameters);
      break;

    case kVelocityCurve:
      ep_set_velocity_curve(value, &parameters);
      break;

    case kVelocityDepth:
      ep_set_velocity_depth(value, &parameters);
      break;

    case kClangVelocityCurve:
      ep_set_clang_velocity_curve(value, &parameters);
      break;

    case kClangVelocityDepth:
      ep_set_clang_velocity_depth(value, &parameters);
      break;

    case kSustainPedal:
      ep_set_sustain_pedal(value, &parameters);
      break;

    default:
      assert(false);
      break;
  }
}

static void set_state_parameters(const ParameterState& state,
                                 ep_parameters_t& parameters,
                                 eq_parameters_t& eq_parameters,
                                 wah_parameters_t& wah_parameters,
                                 tremolo_parameters_t& tremolo_parameters) {
  for (auto &parameter : state.parameters()) {
    set_parameter(parameter.first, parameter.second, parameters, eq_parameters, wah_parameters, tremolo_parameters);
  }
}

static void update_tempo(Steinberg::Vst::ProcessContext* process_context,
                  wah_parameters_t& wah_parameters,
                  tremolo_parameters_t& tremolo_parameters) {
  if ((nullptr != process_context) &&
      (Steinberg::Vst::ProcessContext::kTempoValid & process_context->state)) {
    if (wah_parameters.tempo != process_context->tempo) {
      wah_set_tempo(process_context->tempo, &wah_parameters);
    }
    if (tremolo_parameters.tempo != process_context->tempo) {
      tremolo_set_tempo(process_context->tempo, &tremolo_parameters);
    }
  }
}

static void to_sample32(const double* buffer, const Steinberg::Vst::ProcessData& data) {
  for (auto n = 0; n < data.numSamples; ++n) {
    data.outputs[0].channelBuffers32[0][n] = buffer[n];
    data.outputs[0].channelBuffers32[1][n] = buffer[n];
  }
}

static void to_sample64(const double* buffer, const Steinberg::Vst::ProcessData& data) {
  memcpy(data.outputs[0].channelBuffers64[0], buffer, data.numSamples * sizeof(double));
  memcpy(data.outputs[0].channelBuffers64[1], buffer, data.numSamples * sizeof(double));
}

}  // namespace ep

