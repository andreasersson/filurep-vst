/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILUR_EP_PARAMETER_ID_H
#define FILUR_EP_PARAMETER_ID_H

namespace ep {

typedef enum {
  // Amplifier
  kVolume = 0,
  kDeprecatedOverdrive, // Deprecated

  kEqLowShelfCutoff,
  kEqLowShelfResonance,
  kEqLowShelfGain,
  kEqHighShelfCutoff,
  kEqHighShelfResonance,
  kEqHighShelfGain,

  // Tonebar
  kVelocityCurve = 10,
  kDecay,
  kRelease,
  kSustainPedal,
  kVelocityDepth,

  // Clang
  kClangVolume = 20,
  kClangVelocityCurve,
  kClangDecay,
  kClangVelocityDepth,

  // Pickup
  kPickupTimbre = 30,
  kPickupVolume,

  // Wah
  kWahEnabled = 50,
  kWahCutoff,
  kWahResonance,
  kWahLfoEnabled,
  kWahLfoSpeed,
  kWahTempoSync,
  kWahLowCutoff,
  kWahHighCutoff,
  kWahLfoPhase,

  // Tremolo
  kTremoloEnabled = 70,
  kTremoloLfoSpeed,
  kTremoloTempoSync,
  kTremoloDepth,

} parameter_id_t;

}  // namespace ep

#endif // FILUR_EP_PARAMETER_ID_H
