/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "parameter_state.h"
#include "parameter_ids.h"
#include <filur/ep/ep_parameters.h>
#include <filur/ep/wah_parameters.h>
#include <filur/ep/tremolo_parameters.h>

namespace ep {

static constexpr double min_volume = -40.0;
static constexpr double max_volume = 0.0;
static constexpr double default_volume = -16.0;

static constexpr double min_eq_low_frequency = 40.0;
static constexpr double max_eq_low_frequency = 1000.0;
static constexpr double default_eq_low_frequency = 100.0;

static constexpr double min_eq_hi_frequency = 100.0;
static constexpr double max_eq_hi_frequency = 10000.0;
static constexpr double default_eq_hi_frequency = 1000.0;

static constexpr double min_eq_resonance = 0.5;
static constexpr double max_eq_resonance = 4.0;
static constexpr double default_eq_resonance = 0.707;

static constexpr double min_eq_gain = -12.0;
static constexpr double max_eq_gain = 12.0;
static constexpr double default_eq_gain = 0.0;

static constexpr double tonebar_min_decay = 10e-3;
static constexpr double tonebar_max_decay = 10000e-3;
static constexpr double tonebar_default_decay = 10000e-3;

static constexpr double tonebar_min_release = 70e-3;
static constexpr double tonebar_max_release = 1000e-3;
static constexpr double tonebar_default_release = 70e-3;

static constexpr double min_velocity_curve = -1.0;
static constexpr double max_velocity_curve = 1.0;
static constexpr double min_velocity_depth = 0.0;
static constexpr double max_velocity_depth = 1.0;
static constexpr double tonebar_default_velocity_curve = 0.75;
static constexpr double tonebar_default_velocity_depth = 1.0;
static constexpr double clang_default_velocity_curve = 0.0;
static constexpr double clang_default_velocity_depth = 1.0;

static constexpr double clang_min_volume = -40;
static constexpr double clang_max_volume = 12.0;
static constexpr double clang_default_volume = -6.0;

static constexpr double clang_min_decay = 10e-3;
static constexpr double clang_max_decay = 1000e-3;
static constexpr double clang_default_decay = 400e-3;

static constexpr double pickup_default_timbre = 0.4;
static constexpr double pickup_default_volume = 0.4;

static constexpr double tremolo_min_enabled = TREMOLO_OFF;
static constexpr double tremolo_max_enabled = TREMOLO_ON;

static constexpr double tremolo_min_depth = 0.0;
static constexpr double tremolo_max_depth = 1.0;
static constexpr double tremolo_default_depth = 0.5;

static constexpr double tremolo_min_speed = 0.001;
static constexpr double tremolo_max_speed = 10.0;
static constexpr double tremolo_default_speed = 4.0;

static constexpr int tremolo_min_tempo_sync = TEMPO_SYNC_FREE_RUNNING;
static constexpr int tremolo_max_tempo_sync = TEMPO_SYNC_NUM_TEMPO_SYNCS - 1;

static constexpr double wah_min_enabled = WAH_OFF;
static constexpr double wah_max_enabled = WAH_ON;

static constexpr int wah_min_lfo_enabled = WAH_LFO_OFF;
static constexpr int wah_max_lfo_enabled = WAH_LFO_ON;

static constexpr double wah_min_low_cutoff = 200.0;
static constexpr double wah_max_low_cutoff = 500.0;
static constexpr double wah_default_low_cutoff = 290.0;

static constexpr double wah_min_high_cutoff = 1e3;
static constexpr double wah_max_high_cutoff = 2.5e3;
static constexpr double wah_default_high_cutoff = 1.5e3;

static constexpr double wah_min_cutoff = 0.0;
static constexpr double wah_max_cutoff = 1.0;

static constexpr double wah_min_resonance = 0.5;
static constexpr double wah_max_resonance = 10.0;
static constexpr double wah_default_resonance = 2.0;

static constexpr double wah_min_speed = 0.001;
static constexpr double wah_max_speed = 10.0;
static constexpr double wah_default_speed = 2.0;

static constexpr double wah_min_tempo_sync = TEMPO_SYNC_FREE_RUNNING;
static constexpr double wah_max_tempo_sync = TEMPO_SYNC_NUM_TEMPO_SYNCS - 1;

ParameterState::ParameterState() {
  add(kVolume, default_volume, min_volume, max_volume);

  add(kEqLowShelfCutoff, default_eq_low_frequency, min_eq_low_frequency, max_eq_low_frequency);
  add(kEqLowShelfResonance, default_eq_resonance, min_eq_resonance, max_eq_resonance);
  add(kEqLowShelfGain, default_eq_gain, min_eq_gain, max_eq_gain);
  add(kEqHighShelfCutoff, default_eq_hi_frequency, min_eq_hi_frequency, max_eq_hi_frequency);
  add(kEqHighShelfResonance, default_eq_resonance, min_eq_resonance, max_eq_resonance);
  add(kEqHighShelfGain, default_eq_gain, min_eq_gain, max_eq_gain);

  add(kVelocityCurve, tonebar_default_velocity_curve, min_velocity_curve, max_velocity_curve);
  addSqr(kVelocityDepth, tonebar_default_velocity_depth, min_velocity_depth, max_velocity_depth);
  addSqr(kDecay, tonebar_default_decay, tonebar_min_decay, tonebar_max_decay);
  addSqr(kRelease, tonebar_default_release, tonebar_min_release, tonebar_max_release);

  add(kClangVolume, clang_default_volume, clang_min_volume, clang_max_volume);
  add(kClangVelocityCurve, clang_default_velocity_curve, min_velocity_curve, max_velocity_curve);
  addSqr(kClangVelocityDepth, clang_default_velocity_depth, min_velocity_depth, max_velocity_depth);
  addSqr(kClangDecay, clang_default_decay, clang_min_decay, clang_max_decay);

  add(kPickupTimbre, pickup_default_timbre);
  add(kPickupVolume, pickup_default_volume);

  addUInt(kWahEnabled, WAH_OFF, wah_min_enabled, wah_max_enabled);
  addSqr(kWahCutoff, 0.75, wah_min_cutoff, wah_max_cutoff);
  add(kWahResonance, wah_default_resonance, wah_min_resonance, wah_max_resonance);
  add(kWahLowCutoff, wah_default_low_cutoff, wah_min_low_cutoff, wah_max_low_cutoff);
  add(kWahHighCutoff, wah_default_high_cutoff, wah_min_high_cutoff, wah_max_high_cutoff);
  addUInt(kWahLfoEnabled, WAH_LFO_OFF, wah_min_lfo_enabled, wah_max_lfo_enabled);
  addSqr(kWahLfoSpeed, wah_default_speed, wah_min_speed, wah_max_speed);
  addUInt(kWahTempoSync, TEMPO_SYNC_FREE_RUNNING, wah_min_tempo_sync, wah_max_tempo_sync);
  add(kWahLfoPhase, 0.0, -1.0, 1.0);

  addUInt(kTremoloEnabled, TREMOLO_OFF, tremolo_min_enabled, tremolo_max_enabled);
  add(kTremoloDepth, tremolo_default_depth, tremolo_min_depth, tremolo_max_depth);
  addSqr(kTremoloLfoSpeed, tremolo_default_speed, tremolo_min_speed, tremolo_max_speed);
  addUInt(kTremoloTempoSync, TEMPO_SYNC_FREE_RUNNING, tremolo_min_tempo_sync, tremolo_max_tempo_sync);

  add(kSustainPedal, 0.0);
  runtime(kSustainPedal, true);
}

}  // namespace ep

