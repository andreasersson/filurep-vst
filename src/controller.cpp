/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "controller.h"

#include "parameter_ids.h"
#include "parameter_state.h"
#include "version.h"

#include <filur/ep/wah_parameters.h>
#include <filurvst/common/parameter.h>
#include <filurvst/gui/aboutbox.h>
#include <filurvst/gui/uiviewcreator.h>

#include <pluginterfaces/base/ustring.h>
#include <pluginterfaces/vst/ivstmidicontrollers.h>

#include <vstgui/plugin-bindings/vst3editor.h>
#include <vstgui/uidescription/iviewfactory.h>
#include <vstgui/uidescription/uiattributes.h>

#include <initializer_list>
#include <string>

#ifdef USE_FONT_RESIZE
#include <filurvst/gui/editor.h>
using Editor = filurvst::gui::FontResizeEditor;
#else
using Editor = VSTGUI::VST3Editor;
#endif

namespace ep {

using namespace filurvst;

Steinberg::FUID Controller::cid(0x6E443331, 0x7AA940FD, 0x8FCFC9EA, 0x1676D403);

enum UnitIds {
  kEpUnitId = 1,
  kWahUnitId,
  kTremoloUnitId,
};

static VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            Steinberg::FObject* controller);
static VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                                 const VSTGUI::UIAttributes& attributes,
                                                 const VSTGUI::IUIDescription* description);

Controller::Controller() :
    m_aboutbox(new filurvst::gui::AboutBoxController, false) {
}

Steinberg::tresult Controller::initialize(FUnknown* context) {
  Steinberg::tresult result = EditControllerEx1::initialize(context);
  if (Steinberg::kResultTrue != result) {
    return result;
  }

  std::initializer_list<std::string> sync_names = { "off", "16th", "8th", "4th", "2th", "1", "2", "4" };

  addUnit(new Steinberg::Vst::Unit(Steinberg::String("Ep"), kEpUnitId,
                                   Steinberg::Vst::kRootUnitId,
                                   Steinberg::Vst::kNoProgramListId));
  addUnit(new Steinberg::Vst::Unit(Steinberg::String("Wah"), kWahUnitId,
                                   Steinberg::Vst::kRootUnitId,
                                   Steinberg::Vst::kNoProgramListId));
  addUnit(new Steinberg::Vst::Unit(Steinberg::String("Tremolo"), kTremoloUnitId,
                                   Steinberg::Vst::kRootUnitId,
                                   Steinberg::Vst::kNoProgramListId));

  ParameterState state;

  // Amp
  add_parameter(parameters, state, "Volume", kVolume, kEpUnitId);

  // Eq
  add_range_parameter(parameters, state, "Eq Low Frequency", kEqLowShelfCutoff, kEpUnitId);
  add_parameter(parameters, state, "Eq Low Gain", kEqLowShelfGain, kEpUnitId);
  add_range_parameter(parameters, state, "Eq Hi Frequency", kEqHighShelfCutoff, kEpUnitId);
  add_parameter(parameters, state, "Eq Hi Gain", kEqHighShelfGain, kEpUnitId);

  // Tonebar
  add_parameter(parameters, state, "Velocity Curve", kVelocityCurve, kEpUnitId);
  add_parameter(parameters, state, "Velocity Depth", kVelocityDepth, kEpUnitId);
  add_parameter(parameters, state, "Decay", kDecay, kEpUnitId);
  add_parameter(parameters, state, "Release", kRelease, kEpUnitId);

  add_on_off_parameter(parameters, state, "Sustain Pedal", kSustainPedal, kEpUnitId);

  // Clang
  add_parameter(parameters, state, "Clang Volume", kClangVolume, kEpUnitId);
  add_parameter(parameters, state, "Clang Velocity Curve", kClangVelocityCurve, kEpUnitId);
  add_parameter(parameters, state, "Clang Velocity Depth", kClangVelocityDepth, kEpUnitId);
  add_parameter(parameters, state, "Clang Decay", kClangDecay, kEpUnitId);

  // Pickup
  add_parameter(parameters, state, "Pickup Timbre", kPickupTimbre, kEpUnitId);
  add_parameter(parameters, state, "Pickup Volume", kPickupVolume, kEpUnitId);

  // Wah
  add_on_off_parameter(parameters, state, "Wah Enabled", kWahEnabled, kWahUnitId);
  add_parameter(parameters, state, "Wah Cutoff", kWahCutoff, kWahUnitId);
  add_parameter(parameters, state, "Wah Resonance", kWahResonance, kWahUnitId);
  add_on_off_parameter(parameters, state, "Wah Lfo Enabled", kWahLfoEnabled, kWahUnitId);
  add_parameter(parameters, state, "Wah Speed", kWahLfoSpeed, kWahUnitId);
  add_parameter(parameters, state, "Wah Lfo Phase", kWahLfoPhase, kWahUnitId);
  add_list_parameter(parameters, state, "Wah Tempo Sync", kWahTempoSync, sync_names, kWahUnitId);
  add_range_parameter(parameters, state, "Wah Low Cutoff", kWahLowCutoff, kWahUnitId);
  add_range_parameter(parameters, state, "Wah High Cutoff", kWahHighCutoff, kWahUnitId);

  // Tremolo
  add_on_off_parameter(parameters, state, "Tremolo Enabled", kTremoloEnabled, kTremoloUnitId);
  add_parameter(parameters, state, "Tremolo Speed", kTremoloLfoSpeed, kTremoloUnitId);
  add_list_parameter(parameters, state, "Tremolo Tempo Sync", kTremoloTempoSync, sync_names, kTremoloUnitId);
  add_parameter(parameters, state, "Tremolo Depth", kTremoloDepth, kTremoloUnitId);

  return Steinberg::kResultTrue;
}

Steinberg::tresult Controller::terminate() {
  return EditControllerEx1::terminate();
}

Steinberg::tresult  Controller::setComponentState(Steinberg::IBStream* state_stream) {
  ParameterState state;

  Steinberg::tresult result = state.setState(state_stream);

  if (Steinberg::kResultTrue == result) {
    for (auto &parameter : state.parameters()) {
      setParamNormalized(parameter.first, state.getNormalized(parameter.first));
    }
  }

  return result;
}

Steinberg::tresult Controller::getMidiControllerAssignment(Steinberg::int32 bus_index,
                                                           Steinberg::int16 channel,
                                                           Steinberg::Vst::CtrlNumber midi_cc,
                                                           Steinberg::Vst::ParamID& id) {
  Steinberg::tresult ret_val = Steinberg::kResultFalse;
  Steinberg::Vst::ParamID local_id = Steinberg::Vst::kNoParamId;

  if ((bus_index == 0) && (channel == 0)) {
    switch (midi_cc) {
      case Steinberg::Vst::kCtrlModWheel:
      case Steinberg::Vst::kCtrlFoot:
        local_id = kWahCutoff;
        break;

      case Steinberg::Vst::kCtrlSustainOnOff:
        local_id = kSustainPedal;
        break;
    }
  }

  if (Steinberg::Vst::kNoParamId != local_id) {
    ret_val = Steinberg::kResultTrue;
    id = local_id;
  }
  return ret_val;
}

Steinberg::tresult Controller::openAboutBox(Steinberg::TBool only_check) {
  Steinberg::tresult ret_val = Steinberg::kResultTrue;
  if (!only_check && m_aboutbox) {
    m_aboutbox->open();
  }

  return ret_val;
}

Steinberg::IPlugView* Controller::createView(Steinberg::FIDString name) {
  Steinberg::ConstString local_name(name);
  Steinberg::IPlugView* plug_view = nullptr;

  filurvst::gui::ui_view_creator_init();
  if (Steinberg::Vst::ViewType::kEditor == local_name) {
    plug_view = new Editor(this, "Editor", "filurep.uidesc");
  }

  return plug_view;
}

VSTGUI::CView* Controller::createCustomView(VSTGUI::UTF8StringPtr name,
                                            const VSTGUI::UIAttributes& attributes,
                                            const VSTGUI::IUIDescription* description,
                                            VSTGUI::VST3Editor* /*editor*/) {
  assert(nullptr != description);

  VSTGUI::CView* view = custom_view_about_box(name, attributes, description, m_aboutbox.get());

  if (nullptr == view) {
    view = custom_view_about_box_text(name, attributes, description);
  }

  return view;
}

VSTGUI::COptionMenu* Controller::createContextMenu(const VSTGUI::CPoint& /*pos*/, VSTGUI::VST3Editor* /*editor*/) {
  VSTGUI::COptionMenu* menu = nullptr;

  if (m_aboutbox) {
    std::unique_ptr<VSTGUI::COptionMenu> option_menu(new VSTGUI::COptionMenu());
    std::unique_ptr<VSTGUI::CCommandMenuItem> item(new VSTGUI::CCommandMenuItem(VSTGUI::CCommandMenuItem::Desc("About")));
    item->setActions([aboutbox = m_aboutbox.get()](VSTGUI::CCommandMenuItem*){ aboutbox->open(); });
    option_menu->addEntry(item.release());

    menu = option_menu.release();
  }

  return menu;
}

VSTGUI::CView* custom_view_about_box(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description,
                                     Steinberg::FObject* controller) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBox") == 0)) {
    view = factory->createView(attributes, description);
    filurvst::gui::AboutBox* aboutbox = dynamic_cast<filurvst::gui::AboutBox*>(view);
    if (nullptr != aboutbox) {
      aboutbox->setController(controller);
    }
  }

  return view;
}

VSTGUI::CView* custom_view_about_box_text(VSTGUI::UTF8StringPtr name,
                                     const VSTGUI::UIAttributes& attributes,
                                     const VSTGUI::IUIDescription* description) {
  VSTGUI::CView* view = nullptr;
  const VSTGUI::IViewFactory* factory = description->getViewFactory();
  if ((nullptr != factory) &&
      (nullptr != name) &&
      (strcmp(name, "AboutBoxText") == 0)) {
    std::string about_text;
    about_text += stringPluginName "\n";
    about_text += "version: " FULL_VERSION_STR "\n";
    about_text += stringCompanyWeb"\n\n";
    about_text += stringLegalCopyright"\n";

    about_text += "This program comes with ABSOLUTELY NO WARRANTY.\n";
    about_text += "This is free software, and you are welcome to redistribute it under certain conditions.\n";
    about_text += "You should have received a copy of the GNU General Public License along with " stringPluginName ".\n";
    about_text += "If not, see <https://www.gnu.org/licenses/>.\n";

    VSTGUI::UIAttributes local_attributes = attributes;
    local_attributes.setAttribute("title", about_text);
    view = factory->createView(local_attributes, description);
  }

  return view;
}

}  // namespace ep
