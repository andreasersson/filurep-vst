/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filurep.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurep is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurep.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "../src/parameter_state.cpp"
#include "../src/processor.cpp"

#include <filur/tempo_sync.h>

#include <public.sdk/source/common/memorystream.h>

#include <random>
#include <type_traits>

#define STATE_EXPECT_EQ(state1, state2) \
  for (auto& parameter : state1.parameters()) {\
    filurvst::parameter_info_t info;\
    ASSERT_TRUE(state1.info(parameter.first, info));\
    if (!info.runtime) {\
      EXPECT_EQ(state1.get(parameter.first), state2.get(parameter.first));\
    }\
  }

template <class T> T ep_cast(double value) {
  return static_cast<T>(value);
}


template<> wah_enable_t ep_cast(double value) {
  return ep::value_to_wah_enable(value);
}

template<> wah_lfo_enable_t ep_cast(double value) {
  return ep::value_to_wah_lfo_enable(value);
}

template<> tremolo_enable_t ep_cast(double value) {
  return ep::value_to_tremolo_enable(value);
}

template <> tempo_sync_t ep_cast(double value) {
  return value_to_tempo_sync(value);
}

static void test_setup(filurvst::State& state1,
                       filurvst::State& state2,
                       std::uniform_real_distribution<double> distribution) {
  std::default_random_engine generator;
  Steinberg::MemoryStream stream;
  for (auto& parameter : state1.parameters()) {
    state1.setNormalized(parameter.first, distribution(generator));
  }

  state1.getState(&stream);
  stream.seek(0, Steinberg::IBStream::kIBSeekSet, nullptr);
  state2.setState(&stream);
}

static void test_setup_min(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(0.0, 0.0);
  test_setup(state1, state2, distribution);
}

static void test_setup_max(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(1.0, 1.0);
  test_setup(state1, state2, distribution);
}

static void test_setup_rnd(filurvst::State& state1, filurvst::State& state2) {
  std::uniform_real_distribution<double> distribution(0.0, 1.0);
  test_setup(state1, state2, distribution);
}

template<class T> bool test_set_parameter(ep::ParameterState& state,
                                          Steinberg::Vst::ParamID parameter_id,
                                          ep_parameters_t& parameters,
                                          eq_parameters_t& eq_parameters,
                                          wah_parameters_t& wah_parameters,
                                          tremolo_parameters_t& tremolo_parameters,
                                          T& value) {
  state.setNormalized(parameter_id, 0.0);
  T min_value = ep_cast<T>(state.get(parameter_id));
  ep::set_parameter(parameter_id, min_value, parameters, eq_parameters, wah_parameters, tremolo_parameters);
  T pre_value = value;

  state.setNormalized(parameter_id, 1.0);
  T max_value = ep_cast<T>(state.get(parameter_id));
  ep::set_parameter(parameter_id, max_value, parameters, eq_parameters, wah_parameters, tremolo_parameters);

  bool ret_val = (pre_value != value);

  return ret_val;
}

TEST(ParameterTo, WahEnable) {
  EXPECT_EQ(WAH_OFF, ep::value_to_wah_enable(WAH_OFF));
  EXPECT_EQ(WAH_ON, ep::value_to_wah_enable(WAH_ON));
}

TEST(ParameterTo, WahLfoEnable) {
  EXPECT_EQ(WAH_LFO_OFF, ep::value_to_wah_lfo_enable(WAH_LFO_OFF));
  EXPECT_EQ(WAH_LFO_ON, ep::value_to_wah_lfo_enable(WAH_LFO_ON));
}

TEST(ParameterTo, TremoloEnable) {
  EXPECT_EQ(TREMOLO_OFF, ep::value_to_tremolo_enable(TREMOLO_OFF));
  EXPECT_EQ(TREMOLO_ON, ep::value_to_tremolo_enable(TREMOLO_ON));
}

TEST(ParameterState, GetSetState) {
  ep::ParameterState state1;
  ep::ParameterState state2;

  test_setup_min(state1, state2);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_max(state1, state2);
  STATE_EXPECT_EQ(state1, state2)

  test_setup_rnd(state1, state2);
  STATE_EXPECT_EQ(state1, state2)
}

TEST(Parameter, SetParameter) {
  ep::ParameterState state;
  ep_parameters_t parameters;
  eq_parameters_t eq_parameters;
  wah_parameters_t wah_parameters;
  tremolo_parameters_t tremolo_parameters;
  ep_parameters_init(&parameters);
  eq_parameters_init(&eq_parameters);
  wah_parameters_init(48000.0, &wah_parameters);
  tremolo_parameters_init(48000.0, &tremolo_parameters);

  EXPECT_TRUE(test_set_parameter(state, ep::kVolume, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.volume));

  EXPECT_TRUE(test_set_parameter(state, ep::kEqLowShelfCutoff, parameters, eq_parameters, wah_parameters, tremolo_parameters, eq_parameters.low_shelf.cutoff));
  EXPECT_TRUE(test_set_parameter(state, ep::kEqLowShelfGain, parameters, eq_parameters, wah_parameters, tremolo_parameters, eq_parameters.low_shelf.gain));
  EXPECT_TRUE(test_set_parameter(state, ep::kEqHighShelfCutoff, parameters, eq_parameters, wah_parameters, tremolo_parameters, eq_parameters.high_shelf.cutoff));
  EXPECT_TRUE(test_set_parameter(state, ep::kEqHighShelfGain, parameters, eq_parameters, wah_parameters, tremolo_parameters, eq_parameters.high_shelf.gain));

  EXPECT_TRUE(test_set_parameter(state, ep::kVelocityCurve, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.tonebar_velocity_curve));
  EXPECT_TRUE(test_set_parameter(state, ep::kVelocityDepth, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.tonebar_velocity_depth));
  EXPECT_TRUE(test_set_parameter(state, ep::kDecay, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.tonebar_decay));
  EXPECT_TRUE(test_set_parameter(state, ep::kRelease, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.tonebar_release));
  EXPECT_TRUE(test_set_parameter(state, ep::kSustainPedal, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.sustain_pedal));

  EXPECT_TRUE(test_set_parameter(state, ep::kClangVolume, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.clang_volume));
  EXPECT_TRUE(test_set_parameter(state, ep::kClangVelocityCurve, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.clang_velocity_curve));
  EXPECT_TRUE(test_set_parameter(state, ep::kClangVelocityDepth, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.clang_velocity_depth));
  EXPECT_TRUE(test_set_parameter(state, ep::kClangDecay, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.clang_decay));

  EXPECT_TRUE(test_set_parameter(state, ep::kPickupTimbre, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.pickup_timbre));
  EXPECT_TRUE(test_set_parameter(state, ep::kPickupVolume, parameters, eq_parameters, wah_parameters, tremolo_parameters, parameters.pickup_volume));

  EXPECT_TRUE(test_set_parameter(state, ep::kWahEnabled, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.enabled));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahCutoff, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.cutoff));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahResonance, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.resonance));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahLfoEnabled, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.lfo_enabled));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahLfoSpeed, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.speed));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahTempoSync, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.tempo_sync));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahLowCutoff, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.low_cutoff));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahHighCutoff, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.high_cutoff));
  EXPECT_TRUE(test_set_parameter(state, ep::kWahLfoPhase, parameters, eq_parameters, wah_parameters, tremolo_parameters, wah_parameters.phase));

  EXPECT_TRUE(test_set_parameter(state, ep::kTremoloEnabled, parameters, eq_parameters, wah_parameters, tremolo_parameters, tremolo_parameters.enabled));
  EXPECT_TRUE(test_set_parameter(state, ep::kTremoloLfoSpeed, parameters, eq_parameters, wah_parameters, tremolo_parameters, tremolo_parameters.speed));
  EXPECT_TRUE(test_set_parameter(state, ep::kTremoloTempoSync, parameters, eq_parameters, wah_parameters, tremolo_parameters, tremolo_parameters.tempo_sync));
  EXPECT_TRUE(test_set_parameter(state, ep::kTremoloDepth, parameters, eq_parameters, wah_parameters, tremolo_parameters, tremolo_parameters.depth));
}

TEST(UpdateTempo, NullPointer) {
  wah_parameters_t wah_parameters;
  tremolo_parameters_t tremolo_parameters;
  wah_parameters_init(48000.0, &wah_parameters);
  tremolo_parameters_init(48000.0, &tremolo_parameters);
  wah_parameters.tempo = 90.0;
  tremolo_parameters.tempo = 90.0;

  ep::update_tempo(nullptr, wah_parameters, tremolo_parameters);
  EXPECT_EQ(90.0, wah_parameters.tempo);
  EXPECT_EQ(90.0, tremolo_parameters.tempo);
}

TEST(UpdateTempo, NotTempoValid) {
  wah_parameters_t wah_parameters;
  tremolo_parameters_t tremolo_parameters;
  wah_parameters_init(48000.0, &wah_parameters);
  tremolo_parameters_init(48000.0, &tremolo_parameters);
  wah_parameters.tempo = 0.0;
  tremolo_parameters.tempo = 0.0;
  Steinberg::Vst::ProcessContext process_context;
  process_context.tempo = 120.0;
  process_context.state = 0;

  ep::update_tempo(&process_context, wah_parameters, tremolo_parameters);
  EXPECT_NE(process_context.tempo, wah_parameters.tempo);
  EXPECT_NE(process_context.tempo, tremolo_parameters.tempo);

}

TEST(UpdateTempo, TempoValid) {
  wah_parameters_t wah_parameters;
  tremolo_parameters_t tremolo_parameters;
  wah_parameters_init(48000.0, &wah_parameters);
  tremolo_parameters_init(48000.0, &tremolo_parameters);
  wah_parameters.tempo = 0.0;
  tremolo_parameters.tempo = 0.0;
  Steinberg::Vst::ProcessContext process_context;
  process_context.tempo = 120.0;
  process_context.state = Steinberg::Vst::ProcessContext::kTempoValid;

  ep::update_tempo(&process_context, wah_parameters, tremolo_parameters);
  EXPECT_EQ(process_context.tempo, wah_parameters.tempo);
  EXPECT_EQ(process_context.tempo, tremolo_parameters.tempo);
}

TEST(Process, to_sample32) {
  constexpr size_t num_samples = 512;
  double right_buffer[num_samples];

  float out_left[num_samples];
  float out_right[num_samples];
  float* buffers[] = {out_left, out_right};
  Steinberg::Vst::AudioBusBuffers audio_bus_buffers;
  audio_bus_buffers.channelBuffers32 = buffers;

  Steinberg::Vst::ProcessData process_data;
  process_data.outputs = &audio_bus_buffers;
  process_data.numSamples = num_samples / 2;

  memset(out_left, 0, sizeof(out_left));
  memset(out_right, 0, sizeof(out_left));

  for (size_t n = 0; n < num_samples; ++n) {
    right_buffer[n] = static_cast<double>(n) / num_samples;
  }

  ep::to_sample32(right_buffer, process_data);

  for (int n = 0; n < process_data.numSamples; ++n) {
    EXPECT_EQ(out_left[n], static_cast<float>(right_buffer[n]));
    EXPECT_EQ(out_right[n], static_cast<float>(right_buffer[n]));
  }

  EXPECT_EQ(out_left[process_data.numSamples], 0.0);
  EXPECT_EQ(out_right[process_data.numSamples], 0.0);
}

TEST(Process, to_sample64) {
  constexpr size_t num_samples = 512;
  double right_buffer[num_samples];

  double out_left[num_samples];
  double out_right[num_samples];
  double* buffers[] = {out_left, out_right};
  Steinberg::Vst::AudioBusBuffers audio_bus_buffers;
  audio_bus_buffers.channelBuffers64 = buffers;

  Steinberg::Vst::ProcessData process_data;
  process_data.outputs = &audio_bus_buffers;
  process_data.numSamples = num_samples / 2;

  memset(out_left, 0, sizeof(out_left));
  memset(out_right, 0, sizeof(out_left));

  for (size_t n = 0; n < num_samples; ++n) {
    right_buffer[n] = static_cast<double>(n) / num_samples;
  }

  ep::to_sample64(right_buffer, process_data);

  for (int n = 0; n < process_data.numSamples; ++n) {
    EXPECT_EQ(out_left[n], right_buffer[n]);
    EXPECT_EQ(out_right[n], right_buffer[n]);
  }

  EXPECT_EQ(out_left[process_data.numSamples], 0.0);
  EXPECT_EQ(out_right[process_data.numSamples], 0.0);
}
