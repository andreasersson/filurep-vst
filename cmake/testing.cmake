if(BUILD_TESTS)
  enable_testing()
endif(BUILD_TESTS)

function(add_vst_validator_test target)
  if(BUILD_TESTS)
    get_target_property(PLUGIN_PACKAGE_PATH ${target} SMTG_PLUGIN_PACKAGE_PATH)
    add_dependencies(${target} validator)
    add_test(NAME vstvalidator-${target} COMMAND validator "${PLUGIN_PACKAGE_PATH}" WORKING_DIRECTORY "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
  endif(BUILD_TESTS)
endfunction()

function(add_unit_test target src)
  if(BUILD_TESTS)
    set(properties ${ARGV2})

    if(NOT TARGET ${target})
      add_executable(${target} ${src})
      target_link_libraries(${target} PUBLIC gtest_main filur filur-ep filurvst sdk)
      if(APPLE)
        target_link_libraries(${target} PRIVATE "-framework CoreFoundation")
      endif()

      add_test(NAME ${target} COMMAND ${target})
    else()
      target_sources(${target} PRIVATE ${src})
    endif()

    if(properties AND (NOT properties EQUAL ""))
      set_target_properties(${target} PROPERTIES ${properties})
    endif()

  endif(BUILD_TESTS)
endfunction()
