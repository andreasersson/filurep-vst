// suppress all errors/warnings from external dependencies
*:*vstsdk*/*
*:*gtest*/*
*:*filur-src*/*
*:*filur-vst-src*/*

// suppress missingOverride in controller.h
// since there is a problem with SMTG_OVERRIDE, OBJ_METHODS, DEF_INTERFACE and REFCOUNT_METHODS macros.
missingOverride:*/src/controller.h
